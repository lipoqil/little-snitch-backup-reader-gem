# frozen_string_literal: true

RSpec.describe LittleSnitchBackupReader::Formatter do
  subject(:new_formatter) do
    rules_data = JSON.load_file(File.expand_path(rules_data_path))

    LittleSnitchBackupReader::Formatter.new(
      rules:  rules_data,
      format: LittleSnitchBackupReader::Formatters::AdGuard
    )
  end

  context 'for single remote address' do
    let(:rules_data_path) { 'spec/fixtures/example_rules.json' }

    it 'denies a19.dscg10.akamai.net and edge.microsoft.com' do
      expect(new_formatter.to_s).to eq "||a19.dscg10.akamai.net^\n||edge.microsoft.com^\n"
    end
  end

  context 'for multiple remote address' do
    let(:rules_data_path) { 'spec/fixtures/example_rules_with_remote_address_array.json' }

    it 'denies analytics.google.com, edge.microsoft.com, google-analytics.com and googletagmanager.com' do
      expect(new_formatter.to_s).to eq "||analytics.google.com^\n||edge.microsoft.com^\n||google-analytics.com^\n" \
                                       "||googletagmanager.com^\n"
    end
  end
end
