# frozen_string_literal: true

RSpec.describe LittleSnitchBackupReader::Reader do
  subject(:new_reader) do
    LittleSnitchBackupReader::Reader.new backup_filepaths:
  end

  context 'for a single path' do
    let(:backup_filepaths) { File.expand_path('spec/fixtures/little_snitch_backup.lsbackup') }

    it 'returns three rules for deny action' do
      expect(new_reader.read(action: 'deny').count).to eq 3
    end

    it 'denies a19.dscg10.akamai.net and edge.microsoft.com' do
      denied_hosts = new_reader.read(action: 'deny').map do |rule|
        LittleSnitchBackupReader::Reader.remote_address rule:
      end
      expect(denied_hosts).to include('a19.dscg10.akamai.net', 'datadoghq.com', 'edge.microsoft.com')
    end

    it 'raises an exception for an invalid filepath' do
      expect do
        LittleSnitchBackupReader::Reader.new backup_filepaths: '/a/b/d/e/f/g'
      end.to raise_error ArgumentError, "Path(s) /a/b/d/e/f/g don't exist"
    end
  end

  context 'for multiple paths' do
    let(:backup_filepaths) do
      [File.expand_path('spec/fixtures/little_snitch_backup.lsbackup'),
       File.expand_path('spec/fixtures/another_little_snitch_backup.lsbackup')]
    end

    it 'returns four rules for deny action' do
      expect(new_reader.read(action: 'deny').count).to eq 6
    end

    it 'denies a19.dscg10.akamai.net and edge.microsoft.com' do
      denied_hosts = new_reader.read(action: 'deny').map do |rule|
        LittleSnitchBackupReader::Reader.remote_address rule:
      end
      expect(denied_hosts).to include('a19.dscg10.akamai.net', 'datadoghq.com', 'edge.microsoft.com', 'www.google.com')
    end

    it 'raises an exception for an invalid filepath' do
      expect do
        LittleSnitchBackupReader::Reader.new backup_filepaths: ['/a/b/d/e/f/g', '/h/i/j/k/l/m']
      end.to raise_error ArgumentError, "Path(s) /a/b/d/e/f/g, /h/i/j/k/l/m don't exist"
    end
  end

  context 'for remote address as array' do
    let(:backup_filepaths) do
      File.expand_path('spec/fixtures/little_snitch_backup_with_remote_address_array.lsbackup')
    end

    it 'returns four rules for deny action' do
      expect(new_reader.read(action: 'deny').count).to eq 1
    end

    it 'denies analytics.google.com, google-analytics.com and googletagmanager.com' do
      denied_hosts = new_reader.read(action: 'deny').map do |rule|
        LittleSnitchBackupReader::Reader.remote_address(rule:)
      end.flatten
      expect(denied_hosts).to include('analytics.google.com', 'google-analytics.com', 'googletagmanager.com')
    end

    it 'raises an exception for an invalid filepath' do
      expect do
        LittleSnitchBackupReader::Reader.new backup_filepaths: ['/a/b/d/e/f/g', '/h/i/j/k/l/m']
      end.to raise_error ArgumentError, "Path(s) /a/b/d/e/f/g, /h/i/j/k/l/m don't exist"
    end
  end
end
