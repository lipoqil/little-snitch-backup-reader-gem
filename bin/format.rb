#!/usr/bin/env ruby

require 'bundler/setup'
require 'little_snitch_backup_reader'

if ARGV[0].nil?
  puts '😑 Give some path 😑'
  exit(1)
end

rules_data = LittleSnitchBackupReader::Reader.new(backup_filepaths: ARGV).read(action: 'deny')
rules_formatted = LittleSnitchBackupReader::Formatter.new(rules:  rules_data,
                                                          format: LittleSnitchBackupReader::Formatters::AdGuard)
                                                     .to_s

puts rules_formatted
