# frozen_string_literal: true

require_relative 'lib/little_snitch_backup_reader/version'

Gem::Specification.new do |spec|
  spec.name = 'little_snitch_backup_reader'
  spec.version = LittleSnitchBackupReader::VERSION
  spec.authors = ['Mailo Světel']
  spec.email = ['mailo@rooland.cz']

  spec.summary = 'Tool for reading and transforming JSON backups from the ' \
                 '[Little snitch](https://www.obdev.at/products/littlesnitch/index.html)'
  spec.description = spec.summary
  spec.homepage = 'https://gitlab.com/lipoqil/little-snitch-backup-reader-gem'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.3.0'

  spec.metadata['allowed_push_host'] = 'rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/lipoqil/little-snitch-backup-reader-gem'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/lipoqil/little-snitch-backup-reader-gem'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .gitlab-ci.yml appveyor Gemfile])
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.metadata['rubygems_mfa_required'] = 'true'
end
