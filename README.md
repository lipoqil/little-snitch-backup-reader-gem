# LittleSnitchBackupReader

Tool for reading and transforming JSON backups from the [Little snitch](https://www.obdev.at/products/littlesnitch/index.html)

## Installation

TODO: Replace `UPDATE_WITH_YOUR_GEM_NAME_IMMEDIATELY_AFTER_RELEASE_TO_RUBYGEMS_ORG` with your gem name right after releasing it to RubyGems.org. Please do not do it earlier due to security reasons. Alternatively, replace this section with instructions to install your gem from git if you don't plan to release to RubyGems.org.

Install the gem and add to the application's Gemfile by executing:

    $ bundle add little_snitch_backup_reader

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install little_snitch_backup_reader

## Usage

Example
```ruby
require 'bundler/setup'
require 'little_snitch_backup_reader'

rules_data = LittleSnitchBackupReader::Reader.new(backup_filepaths: ARGV).read(action: 'deny')
rules_formatted = LittleSnitchBackupReader::Formatter.new(rules:  rules_data,
                                                          format: LittleSnitchBackupReader::Formatters::AdGuard)
                                                     .to_s

puts rules_formatted
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/little_snitch_backup_reader.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
