# frozen_string_literal: true

require_relative 'formatter_base'

module LittleSnitchBackupReader
  module Formatters
    # Rules formatter for AdGuard
    #
    # @see AdGuard rules https://github.com/AdguardTeam/AdGuardHome/wiki/Hosts-Blocklists
    class AdGuard < Formatters::FormatterBase
      RULE_SYNTAX = {
        'deny' => '||%s^',
        'allow' => '@@||%s^',
        'suggestion' => '# ||%s^'
      }.freeze

      def formatted_rules
        unique_rules = @rules.each_with_object(Set.new, &method(:compile_rules))

        unique_rules.sort_by { |rule_line| rule_line.split('.').reverse[1..] }.join
      end

      private

      def compile_rules(rule, rules_list)
        address = LittleSnitchBackupReader::Reader.remote_address(rule:)
        if address.is_a? String
          rules_list.add "#{RULE_SYNTAX[rule['action']] % address}\n"
        elsif address.is_a? Array
          address.each do |sub_address|
            rules_list.add "#{RULE_SYNTAX[rule['action']] % sub_address}\n"
          end
        end
      end
    end
  end
end
