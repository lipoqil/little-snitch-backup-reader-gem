# frozen_string_literal: true

module LittleSnitchBackupReader
  module Formatters
    # Common functionality for all formatters
    class FormatterBase
      def initialize(rules:)
        @rules = rules
      end
    end
  end
end
