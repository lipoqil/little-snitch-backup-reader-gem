# frozen_string_literal: true

require 'json'

# Toolset for working with Little snitch backups in Ruby
module LittleSnitchBackupReader
  # Reads contents of a backup into a Ruby structure
  class Reader
    attr_reader :rules_with_hosts, :logger

    # @param [String|Array<String>] backup_filepaths
    def initialize(backup_filepaths:)
      backup_filepaths = Array(backup_filepaths) unless backup_filepaths.is_a?(Array)
      wrong_paths = backup_filepaths.reject { |backup_filepath| File.exist?(backup_filepath) }
      raise ArgumentError, "Path(s) #{wrong_paths.join ', '} don't exist" unless wrong_paths.empty?

      @backup_filepaths = backup_filepaths
      @rules_with_hosts = nil
    end

    # @param [String] action
    #
    # @return [Array<Hash{String (frozen)->String (frozen)}>]
    def read(action:)
      if @rules_with_hosts.nil?
        @backup_filepaths.each do |backup_filepath|
          prepare_rules_with_hosts backup_filepath:
        end
      end

      @rules_with_hosts[action]
    end

    def self.remote_address(rule:, only_first: false)
      address = rule['remote-hosts'] || rule['remote-domains']

      case address
      when String
        address
      when Array
        only_first ? address.first : address
      end
    end

    private

    def prepare_rules_with_hosts(backup_filepath:)
      backup = JSON.load_file! backup_filepath

      @rules_with_hosts ||= {}
      backup['rules'].each(&method(:compile_rules))

      @rules_with_hosts.each_key(&method(:sort_actions))
    end

    def sort_actions(rule_action)
      @rules_with_hosts[rule_action].sort_by! do |rule|
        self.class.remote_address(rule:, only_first: true).gsub('^www\.', '')
      end
    end

    def compile_rules(rule)
      return if self.class.remote_address(rule:).nil? || self.class.remote_address(rule:).empty?

      @rules_with_hosts[rule['action']] ||= []
      @rules_with_hosts[rule['action']] << rule
    end
  end
end
