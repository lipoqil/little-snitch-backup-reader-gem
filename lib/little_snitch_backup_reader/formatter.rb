# frozen_string_literal: true

require_relative 'formatters/ad_guard'

module LittleSnitchBackupReader
  # Functionality for formatting rules
  class Formatter
    attr_reader :rules, :format

    def initialize(rules:, format:)
      @rules = rules
      @format = format
    end

    def to_s
      format
        .new(rules:)
        .formatted_rules
    end
  end
end
