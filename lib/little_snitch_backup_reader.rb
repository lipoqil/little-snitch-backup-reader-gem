# frozen_string_literal: true

require_relative 'little_snitch_backup_reader/version'
require_relative 'little_snitch_backup_reader/reader'
require_relative 'little_snitch_backup_reader/formatter'

# Toolset for working with Little snitch backups in Ruby
module LittleSnitchBackupReader
  class Error < StandardError; end
end
